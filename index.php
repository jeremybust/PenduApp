<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>PenduApp</title>
  <style>
    * {
      font-family: Roboto;
      font-size: 18px;
    }

  header {
    background-color: #EEEEEE;
    color: white;
    width: 100%;
    height: 100px;
    border-radius: 10px;
  }

  h1, h2 {
    color: #2C3E50;
  }

  h1 {
    margin: 0;
    text-align: center;
    padding: 35px;
    font-size: 25px;
  }

  h2 {
    text-align: center;
  }

  .mots {
      float: left;
      margin-left: 20px;
  }

  .joueurs {
      float: right;
      margin-right: 20px;
  }

  .mots, .joueurs {
    border: 1px solid transparent;
    border-radius: 5px;
    margin-top: 20px;
    background-color: #EEEEEE;
  }

  a {
    border: 1px solid transparent;
    text-decoration: none;
    color: #2C3E50;
    border-radius: 1px;
  }

  a:hover {
    font-weight: 700;
  }

  ul {
    list-style-type: none;
  }


  @media screen and (min-width: 768px) and (max-width: 1199px) {
    .mots {
        float: left;
        margin-left: 0;
        margin-right: 50px;
    }

    .joueurs {
        float: left;
        margin-right: 0;
    }
  }

  @media  screen and (max-width: 767px) {
    h1 {
      margin: 0;
    }

    .mots {
        margin-left: 0;
    }

    .joueurs {
        margin-right: 0;
    }
  }


  </style>
<body>
  <header>
    <h1>PenduApp</h1>
  </header>
  <div class="mots">
    <h2>Mots</h2>
<ul>
<?php
 $handle = mysqli_connect("localhost","root","JeveuxPop01!","PenduApp");
 $query = "SELECT * FROM Mots";
 $result = mysqli_query($handle,$query);
 while($line = mysqli_fetch_array($result)) {
   echo "\t<li>";
   echo "[" . $line["ID"] ."] ";
   echo $line["Mot"];
   echo "&nbsp;<a href=delete_mot.php?ID=" . $line["ID"] . ">Supprimer</a>";
   echo "&nbsp;<a href=update_mot1.php?ID=" . $line["ID"] . ">Modifier</a>";
   echo "</li>\n";

 }
?>
</ul>

<form action="create_mot.php" method="post">
  <label for="Mot"></label>
  <input name="Mot" type="text" placeholder="Entrez le nouveau mot">
  <input type="submit">
</form>
</div>
<div class="joueurs">
  <h2>Joueurs</h2>
<ul>
<?php
 $handle = mysqli_connect("localhost","root","JeveuxPop01!","PenduApp");
 $query = "SELECT * FROM Joueurs";
 $result = mysqli_query($handle,$query);
 while($line = mysqli_fetch_array($result)) {
     echo "\t<li>";
     echo "[" . $line["ID"] ."] ";
     echo $line["Nom"];
     echo "&nbsp;<a href=delete_joueur.php?ID=" . $line["ID"] . ">Supprimer</a>";
     echo "&nbsp;<a href=update_joueur1.php?ID=" . $line["ID"] . ">Modifier</a>";
     echo "</li>\n";
}

?>
</ul>
<form action="create_joueur.php" method="post">
  <label for="Nom"></label>
  <input name="Nom" type="text" placeholder="Entrez le nouveau joueur">
  <input type="submit">
</form>
</div>

</body>
</html>
